//dependencies
const Discord = require('discord.js');
const Commands1 = require('./commands.js');
const fs = require('fs');
//create a discord instance and login
const tokenFile = JSON.parse(fs.readFileSync('key.txt', 'utf8'));
const token = tokenFile['1'];
const client = new Discord.Client();
client.login(token);

const mainChannel = '707219714400845844';

client.on('guildMemberAdd', member => {
    console.log(member+' has joined!');
    client.channels.get(mainChannel).send(
        "<@" + member.id + "> , добро пожаловать в Радиочан!"
    );
});


//==================================================================================================================================
//Command listening
//==================================================================================================================================

let cooldownUsers = new Set();

client.on('message', (receivedMessage) => {
    if (receivedMessage.author == client.user) { // Prevent bot from responding to its own messages
        return
    }
    if (receivedMessage.content.startsWith("!")/*&&(!receivedMessage.member.roles.has(banRoleId))*/) {
            processCommand(receivedMessage);
    }
    const regex = RegExp('[oоOОщЩJj](?![7]$)\\d+$');
    if(regex.test(receivedMessage.content))
    {
        receivedMessage.channel.send("Бля, думаешь ты самый умный, да?").then(msg => { msg.delete(10000)});
        return;
    }
    
});

//==================================================================================================================================
//Command processing
//==================================================================================================================================

function processCommand(receivedMessage) {
    let fullCommand = receivedMessage.content.substr(1) // Remove the leading exclamation mark
    let splitCommand = fullCommand.split(" ") // Split the message up in to pieces for each space
    let primaryCommand = splitCommand[0].toLowerCase(); // The first word directly after the exclamation is the command
    let args = splitCommand.slice(1) // All other words are arguments/parameters/options for the command


    console.log("Command received: " + primaryCommand)
    console.log("Requested by: " + receivedMessage.author.username)
    console.log("Arguments: " + args) // There may not be any arguments

    if (primaryCommand == "help" || primaryCommand == "помощь") {
        try {
            Commands1.helpCommand(args, receivedMessage, Discord)
        } catch (e) {
            console.log(e.stack);
        }
    } else if (primaryCommand == "like" || primaryCommand == "лойс")
    {
        try {
            Commands1.loythCommand(receivedMessage,args)
        } catch (e) {
            console.log(e.stack);
        }
    }
}