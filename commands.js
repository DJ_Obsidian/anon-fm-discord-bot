//Settings
const pookCoolDownTime = 24*60*60; //24 hrs in sec
const maxBanTime = 24*31; //Max Ban Time in hrs
const helpDisplayTime = 120;//Sec
const shortDeleteTime = 10;//Sec
const pressFCoolDownTime = 30*60;//Sec, aka 30 mins
const SaluteCoolDownTime = 30*60;
const embedColor = "#0099ff";


exports.helpCommand=(args, receivedMessage, Discord)=> {
    const embed = new Discord.RichEmbed()
        .setColor(embedColor)
        .setTitle('Помощь, угу...')
        .setThumbnail("https://media.discordapp.net/attachments/595920342141370381/596022380628017173/O7_12.png")
        .addField('!help/!помощь', '`Вывести это сообщение, конечно же.`')
        .addField('!пук/!ger', '`Пук случайному человеку. Работает только в `<#596029543136100392>` КД - сутки.`')
        .addField('!лойс/!лукас/!лайк/!like', '`Накидать лукасов сообщению. Использовать: !лойс <id сообщения/оставить пустым если лойснуть последнее сообщение в канале>`')

    receivedMessage.channel.send(embed)
        .then(msg => {
            msg.delete(helpDisplayTime*1000)
        })
        .catch();
    receivedMessage.delete(shortDeleteTime*1000)
}


exports.loythCommand=(receivedMessage,cmdArgs) => {
    if(cmdArgs[0]!==undefined){
        receivedMessage.channel.fetchMessages(cmdArgs[0])
            .then(msgToLoyth =>{
                if (msgToLoyth.reactions!==undefined){
                    msgToLoyth.react('👍');
                    msgToLoyth.react('👍🏻');
                    msgToLoyth.react('👍🏼');
                    msgToLoyth.react('👍🏽');
                    msgToLoyth.react('👍🏾');
                    msgToLoyth.react('👍🏿');
                    receivedMessage.delete(shortDeleteTime*1000);
                }
                else {
                    receivedMessage.channel.send('Эмодзи уже стоят').then(msg => { msg.delete(shortDeleteTime*1000)}).catch();
                    receivedMessage.delete(shortDeleteTime*1000);
                }
            })
            .catch(err=>{
                console.error(err);
                receivedMessage.channel.send('Сообщение с данным айди не найдено').then(msg => { msg.delete(shortDeleteTime*1000)}).catch();
                receivedMessage.delete(shortDeleteTime*1000);
            });
    }else{
        receivedMessage.channel.fetchMessages({limit:2})
            .then(messages => {
                let msgToLoyth=messages.array()[1];
                if (msgToLoyth.reactions!==undefined){
                    msgToLoyth.react('👍');
                    msgToLoyth.react('👍🏻');
                    msgToLoyth.react('👍🏼');
                    msgToLoyth.react('👍🏽');
                    msgToLoyth.react('👍🏾');
                    msgToLoyth.react('👍🏿');
                    receivedMessage.delete(shortDeleteTime*1000);
                }
                else {
                    receivedMessage.channel.send('Эмодзи уже стоят').then(msg => { msg.delete(shortDeleteTime*1000)}).catch();
                    receivedMessage.delete(shortDeleteTime*1000);
                }
            })
            .catch(err=>{
                console.error(err);
                receivedMessage.channel.send('Не удалось получить сообщение').then(msg => { msg.delete(shortDeleteTime*1000)}).catch();
                receivedMessage.delete(shortDeleteTime*1000);
            });
    }
}

